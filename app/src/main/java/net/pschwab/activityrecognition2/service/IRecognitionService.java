package net.pschwab.activityrecognition2.service;

/**
 * The interface to recognition services.
 *
 * NOTE: Predictions are returned asynchronously via listener events to enable
 * the use of long running tasks on the background thread.
 */
public interface IRecognitionService {
    interface IRecognitionServiceListener {
        void hasReceivedPredictions(Float[] probabilities);
        void failedToPredictProbabilities(Exception ex);
    }

    void setListener(IRecognitionServiceListener listener);
    void predictClassificationProbabilities(float[] x, float[] y, float[] z);
}
