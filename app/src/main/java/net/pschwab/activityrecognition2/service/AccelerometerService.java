package net.pschwab.activityrecognition2.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;

import java.util.Arrays;

/**
 * The __AccelerometerService__ provides access to the device's accelerometer readings.
 * Its functionality can be accessed through an Android IBinder interface.
 */
public class AccelerometerService extends Service implements SensorEventListener {
    public static final int RECOGNITION_TIME_IN_MS = 10 * 1000; // 10 s recognition time.
    public static final int NUM_MEASUREMENTS_PER_CYCLE = 200;   // 50 ms interval

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private int mMeasurementCount = NUM_MEASUREMENTS_PER_CYCLE;
    private float[] mXMeasurements = new float[NUM_MEASUREMENTS_PER_CYCLE];
    private float[] mYMeasurements = new float[NUM_MEASUREMENTS_PER_CYCLE];
    private float[] mZMeasurements = new float[NUM_MEASUREMENTS_PER_CYCLE];

    public class LocalBinder extends Binder {
        public AccelerometerService getInstance() {
            return AccelerometerService.this;
        }
    }

    public AccelerometerService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Calculate the required rate of sensor readings
        // to hit the specified number of measurements per cycle.
        int sensorRate = RECOGNITION_TIME_IN_MS / NUM_MEASUREMENTS_PER_CYCLE;

        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Sensor rate is converted to micro seconds.
        mSensorManager.registerListener(this, mAccelerometer, sensorRate * 1000);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSensorManager.unregisterListener(this, mAccelerometer);
    }

    /**
     * Initiates recording of a full cycle. Can not be cancelled but can be reset.
     */
    public void startRecording() {
        mMeasurementCount = 0;
    }

    /**
     * Returns whether or not the service has finished recording a full measurement cycle.
     *
     * @return A flag indicating whether a full measurement cycle has been recorded.
     */
    public boolean hasFinishedRecording() {
        return mMeasurementCount >= NUM_MEASUREMENTS_PER_CYCLE;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (mMeasurementCount < NUM_MEASUREMENTS_PER_CYCLE) {
            recordValues(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
            mMeasurementCount++;
        }
    }

    /**
     * Records the next measurement triplet.
     *
     * @param x The next x acceleration measurement.
     * @param y The next y acceleration measurement.
     * @param z The next z acceleration measurement.
     */
    private void recordValues(float x, float y, float z) {
        mXMeasurements[mMeasurementCount] = x;
        mYMeasurements[mMeasurementCount] = y;
        mZMeasurements[mMeasurementCount] = z;
    }

    public float[] getXMeasurements() {
        return Arrays.copyOfRange(mXMeasurements, 0, mMeasurementCount);
    }

    public float[] getYMeasurements() {
        return Arrays.copyOfRange(mYMeasurements, 0, mMeasurementCount);
    }

    public float[] getZMeasurements() {
        return Arrays.copyOfRange(mZMeasurements, 0, mMeasurementCount);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // Unused.
    }
}
