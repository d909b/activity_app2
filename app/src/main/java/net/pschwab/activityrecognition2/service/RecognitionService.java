package net.pschwab.activityrecognition2.service;

import android.os.AsyncTask;
import android.util.Log;

import net.pschwab.activityrecognition2.ServiceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * The __RecognitionService__ class provides access to the activity recognition REST service.
 * Complete measurements are sent to the server and evaluated on the trained model.
 * The resulting prediction is returned to the caller.
 */
public class RecognitionService implements IRecognitionService {
    private static final String TAG = RecognitionService.class.getCanonicalName();

    private static final String ENDPOINT =
            "http://ec2-52-53-148-184.us-west-1.compute.amazonaws.com:12000/api/v1";
    private static final String METHOD_RECOGNIZE_ACTIVITY = "/recognizeActivity";
    private static final MediaType MEDIA_TYPE_JSON
            = MediaType.parse("application/json; charset=utf-8");

    private OkHttpClient mClient;
    private IRecognitionServiceListener mListener;

    public RecognitionService() {
        mClient = new OkHttpClient();
    }

    @Override
    public void setListener(IRecognitionServiceListener listener) {
        mListener = listener;
    }

    /**
     * The __PredictionTask__ class executes a single prediction task asynchronously.
     */
    private class PredictionTask extends AsyncTask<Void, Void, Float[]> {
        private float[] mX, mY, mZ;
        private Exception mError;

        public PredictionTask(float[] x, float[] y, float[] z) {
            mX = x;
            mY = y;
            mZ = z;
        }

        /**
         * Builds the request body from the measurements attached to the task. The request body is
         * in JSON format.
         *
         * @return A JSON String corresponding to the request body of this task.
         * @throws JSONException Throws if the request body can not built.
         */
        private String buildRequestBody() throws JSONException {
            assert mX.length == mY.length;
            assert mY.length == mZ.length;

            JSONObject root = new JSONObject();
            JSONObject records = new JSONObject();
            JSONArray x = new JSONArray(), y = new JSONArray(), z = new JSONArray();

            // Populate the JSON array with the measurement data.
            // NOTE: Constructor from primitive objects is only available from API level 23 onwards.
            for(int i = 0; i < mX.length; ++i) {
                x.put(i, mX[i]);
                y.put(i, mY[i]);
                z.put(i, mZ[i]);
            }

            // Cast is necessary because the JSONObject API misinterprets JSONArrays.
            records.put("x", (Object)x);
            records.put("y", (Object)x);
            records.put("z", (Object)x);
            root.put("records", records);

            return root.toString();
        }

        /**
         * Calls the recognition REST web service on a background thread.
         *
         * @param voids Unused.
         * @return The received response or null if an error occured.
         *         In case of an error, the root exception will be passed to the attached listener.
         */
        @Override
        protected Float[] doInBackground(Void... voids) {
            RequestBody body;
            try {
                body = RequestBody.create(MEDIA_TYPE_JSON, buildRequestBody());
            } catch (JSONException ex) {
                // Could not prepare request.
                mError = ex;
                return null;
            }

            Request request = new Request.Builder()
                    .url(ENDPOINT + METHOD_RECOGNIZE_ACTIVITY)
                    .post(body)
                    .build();

            String responseString;
            try {
                Response response = mClient.newCall(request).execute();
                responseString = response.body().string();
            } catch(IOException ex) {
                // Could not connect.
                mError = ex;
                return null;
            }

            try {
                // Parse the API response from JSON format.
                JSONObject json = new JSONObject(responseString);
                JSONArray results = json.getJSONArray("results");

                int len = results.length();
                Float[] retVal = new Float[len];
                for(int i = 0; i < len; ++i) {
                    retVal[i] = (float)results.getDouble(i);
                }
                return retVal;
            } catch (JSONException ex) {
                // Parsing failed - wrong response format.
                mError = ex;
                return null;
            }
        }

        /**
         * Dispatches the async task results to the listener on the main thread.
         *
         * @param probabilities The calculated class probabilities.
         */
        @Override
        protected void onPostExecute(Float[] probabilities) {
            if(mListener == null) {
                return;
            }

            if(mError != null) {
                mListener.failedToPredictProbabilities(mError);
            } else {
                mListener.hasReceivedPredictions(probabilities);
            }
        }
    }

    /**
     * Calls the recognition web service to retrieve class probabilities based on the sensor
     * readings __x__, __y__ and __z__ for activity recognition.
     *
     * Prediction results are returned via listener events.
     *
     * @param x 200 sensor readings for acceleration on the x-axis.
     * @param y 200 sensor readings for acceleration on the y-axis.
     * @param z 200 sensor readings for acceleration on the z-axis.
     */
    @Override
    public void predictClassificationProbabilities(float[] x, float[] y, float[] z) {
        if(mListener != null) {
            if(x.length != AccelerometerService.NUM_MEASUREMENTS_PER_CYCLE ||
               y.length != AccelerometerService.NUM_MEASUREMENTS_PER_CYCLE ||
               z.length != AccelerometerService.NUM_MEASUREMENTS_PER_CYCLE) {
                // Notify the listener of length mismatches.
                mListener.failedToPredictProbabilities(
                        new Exception(String.format(
                                Locale.US,
                                "Classification requires exactly %1$d sensor readings.",
                                AccelerometerService.NUM_MEASUREMENTS_PER_CYCLE
                        )
                ));
            } else {
                ServiceUtil.executeAsyncTask(new PredictionTask(x, y, z));
            }
        } else {
            // Do not execute if the result will not be handled.
            Log.e(TAG, "Tried to execute async task" +
                    " without attaching a listener to handle the result.");
        }
    }
}
