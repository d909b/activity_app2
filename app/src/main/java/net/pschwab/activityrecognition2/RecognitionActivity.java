package net.pschwab.activityrecognition2;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import net.pschwab.activityrecognition2.service.AccelerometerService;
import net.pschwab.activityrecognition2.service.IRecognitionService;
import net.pschwab.activityrecognition2.service.RecognitionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The __RecognitionActivity__ is the view controller to the activity recognition view.
 * It implements and coordinates the application logic.
 * (@see activity_recognition.xml)
 */
public class RecognitionActivity extends AppCompatActivity implements
        IRecognitionService.IRecognitionServiceListener {
    private static final String TAG = RecognitionActivity.class.getCanonicalName();
    private static final int RAMP_UP_TIME_IN_MS = 2000;

    private Timer mProgressTimer = new Timer();

    private enum RecognitionState {
        RECOGNITION_STATE_WAIT, RECOGNITION_STATE_RAMP_UP, RECOGNITION_STATE_RUN
    }
    private RecognitionState mState = RecognitionState.RECOGNITION_STATE_WAIT;

    private LinearLayout mTextLog;
    private LineChart mLineChart;
    private LimitLine mLimitLine;
    private TextView mWalkingText;
    private TextView mRunningText;
    private int mDefaultTextColor;
    private AccelerometerService mAccelerometerService;
    private IRecognitionService mRecognitionService = new RecognitionService();

    // We connect to the accelerometer service via Android's IBinder interface.
    ServiceConnection mConnection = new ServiceConnection() {
        // Called once the accelerometer service is ready to fulfil requests.
        public void onServiceConnected(ComponentName name, IBinder service) {
            writeToLog("Connected to AccelerometerService.");
            AccelerometerService.LocalBinder mLocalBinder =
                    (AccelerometerService.LocalBinder)service;
            mAccelerometerService = mLocalBinder.getInstance();
        }

        // Called on service shutdown.
        public void onServiceDisconnected(ComponentName name) {
            writeToLog("Disconnected from AccelerometerService.");
            mAccelerometerService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recognition);
        setTitle("Activity Recognition");

        // Connect button events.
        Button btnStart = (Button)findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickedStartRecording();
            }
        });
        Button btnToggleLog = (Button)findViewById(R.id.btnToggleLog);
        btnToggleLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickedToggleLog();
            }
        });

        // Bind to output fields.
        mWalkingText = (TextView)findViewById(R.id.tvWalkingClass);
        mRunningText = (TextView)findViewById(R.id.tvRunningClass);
        mTextLog = (LinearLayout)findViewById(R.id.llLog);
        mDefaultTextColor = mWalkingText.getCurrentTextColor();

        // Set up the dynamic sensor chart.
        mLineChart = (LineChart)findViewById(R.id.chart);
        mLineChart.setBackgroundColor(getResources().getColor(android.R.color.white));
        mLineChart.setAutoScaleMinMaxEnabled(false);
        mLineChart.getXAxis().setAxisMinimum(0);
        mLineChart.getXAxis().setAxisMaximum(200);
        mLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mLineChart.getAxisLeft().setAxisMinimum(-20);
        mLineChart.getAxisLeft().setAxisMaximum(20);
        mLineChart.getAxisRight().setAxisMinimum(-20);
        mLineChart.getAxisRight().setAxisMaximum(20);
        Description desc = new Description();
        desc.setText("Accelerations in m/s^2");
        mLineChart.setDescription(desc);
        mLineChart.setNoDataText("Begin by pressing 'Start Recording' below.");
        mLineChart.setNoDataTextColor(Color.RED);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Bind to the AccelerometerService.
        startService(new Intent(this, AccelerometerService.class));
        bindService(new Intent(this, AccelerometerService.class), mConnection, BIND_AUTO_CREATE);

        // Listen to recognitions.
        mRecognitionService.setListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unbind from the AccelerometerService.
        if(mAccelerometerService != null) {
            unbindService(mConnection);
        }

        // Unregister from the recognition service.
        mRecognitionService.setListener(null);
    }

    /**
     * Prints a message to the console and appends the message to a toggleable UI view.
     *
     * @param msg The message to be printed.
     */
    private void writeToLog(final String msg) {
        Log.i(TAG, msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tv = new TextView(RecognitionActivity.this);
                LinearLayout.LayoutParams lp =
                        new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                                      LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(8, 0, 8, 8);
                tv.setLayoutParams(lp);
                tv.setText(msg);
                mTextLog.addView(tv);
            }
        });
    }

    private void resetClassificationProbabilities() {
        mWalkingText.setText("-- %");
        mRunningText.setText("-- %");
    }

    private void onClickedStartRecording() {
        if(mState != RecognitionState.RECOGNITION_STATE_WAIT) {
            // Do not start another recording before the last one is finished.
            return;
        }

        // The user is given a short ramp up time to get ready to perform an activity.
        mState = RecognitionState.RECOGNITION_STATE_RAMP_UP;
        writeToLog("Initiated ramp up timer.");

        // Load ramp up time from configuration.
        final int rampUpTime = RAMP_UP_TIME_IN_MS;
        // Define how often the dynamic sensor chart is updated (in ms).
        final int updateTime = 250;

        // Inform the user that the ramp up time has begun.
        Toast.makeText(this,
                String.format(Locale.US,
                              "Recording will start in %1$.2f seconds.",
                              rampUpTime / 1000.f),
                Toast.LENGTH_SHORT
        ).show();

        resetClassificationProbabilities();

        // Start updating the dynamic sensor chart every __updateTime__ ms after a __rampUpTime__.
        mProgressTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(mState == RecognitionState.RECOGNITION_STATE_RAMP_UP) {
                    // Measurements start after the ramp up time to give the user time to start
                    // performing an activity.
                    mAccelerometerService.startRecording();
                    mState = RecognitionState.RECOGNITION_STATE_RUN;
                    writeToLog("Started recording.");
                }

                updateChart();

                if(mAccelerometerService.hasFinishedRecording()) {
                    // Reset the state once a full recording cycle has been completed.
                    mProgressTimer.cancel();
                    mProgressTimer = new Timer();
                    mState = RecognitionState.RECOGNITION_STATE_WAIT;
                    writeToLog("Completed recording.");

                    // Initiate a classification.
                    performClassification();
                }
            }
        }, rampUpTime, updateTime);
    }

    private void onClickedToggleLog() {
        if(mTextLog.getVisibility() == View.GONE) {
            mTextLog.setVisibility(View.VISIBLE);
        } else {
            mTextLog.setVisibility(View.GONE);
        }
    }

    private LineDataSet createLineDataSet(String label, float[] values, int color) {
        LineDataSet set = new LineDataSet(packReadings(values), label);
        set.setColor(getResources().getColor(color));
        set.setDrawValues(false);
        set.setDrawCircles(false);
        set.setLineWidth(2);
        return set;
    }

    private void updateChart() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float[] x = mAccelerometerService.getXMeasurements(),
                        y = mAccelerometerService.getYMeasurements(),
                        z = mAccelerometerService.getZMeasurements();

                LineData data = new LineData(
                        createLineDataSet("x", x, R.color.colorGraphBlue),
                        createLineDataSet("y", y, R.color.colorGraphGreen),
                        createLineDataSet("z", z, R.color.colorGraphRed)
                );

                // Include a running limit line.
                if(mLimitLine != null) {
                    mLineChart.getXAxis().removeLimitLine(mLimitLine);
                }
                mLimitLine = new LimitLine(x.length, "");
                mLimitLine.setLineWidth(4);
                mLimitLine.setLineColor(Color.BLACK);

                mLineChart.getXAxis().addLimitLine(mLimitLine);
                mLineChart.setData(data);
                mLineChart.invalidate();
            }
        });
    }

    private List<Entry> packReadings(float[] measurements) {
        ArrayList<Entry> retVal = new ArrayList<>();
        for(int i = 0; i < measurements.length; ++i) {
            retVal.add(new Entry(i, measurements[i]));
        }
        return retVal;
    }

    private void performClassification() {
        float[] x = mAccelerometerService.getXMeasurements(),
                y = mAccelerometerService.getYMeasurements(),
                z = mAccelerometerService.getZMeasurements();

        writeToLog("Contacting recognition web service.");
        mRecognitionService.predictClassificationProbabilities(x, y, z);
    }

    @Override
    public void hasReceivedPredictions(Float[] probabilities) {
        writeToLog(String.format(Locale.US, "Received %1$d predictions", probabilities.length));

        if(probabilities.length == 2) {
            mWalkingText.setText(String.format(Locale.US, "%1$.2f %%", probabilities[0] * 100.));
            mRunningText.setText(String.format(Locale.US, "%1$.2f %%", probabilities[1] * 100.));

            if(probabilities[0] > probabilities[1]) {
                mRunningText.setTextColor(mDefaultTextColor);
                mWalkingText.setTextColor(getResources().getColor(R.color.colorGraphGreen));
            } else {
                mRunningText.setTextColor(getResources().getColor(R.color.colorGraphGreen));
                mWalkingText.setTextColor(mDefaultTextColor);
            }
        } else {
            Log.e(TAG, "Received a wrong number of prediction classes.");
        }
    }

    @Override
    public void failedToPredictProbabilities(Exception ex) {
        writeToLog(String.format(Locale.US, "Received error: %1$s", ex.getMessage()));

        // Inform the user that the classification could not be completed.
        Toast.makeText(this,
                        "Recognition failed, because the web service could not be contacted." +
                        " (See log for details)",
                        Toast.LENGTH_LONG
        ).show();
    }
}
